<img src="./docs/imgs/20220108o1727.snowwhiteportrait.v1.png" align="right" width="166" height="207" data-dims="x500y621" style="margin-left:1.7em;" alt="Schneewittchen Portrait">

# Sneewittchen Solution &nbsp; <sup>*v0.1.3*</sup>

This project demonstrates **separation of user interface from functionality**
 in C++. One and the same functionality is handled by multiple different
 user interfaces. The project shall show, how to wire together a CPP library,
 a console UI and graphical UI.

## Synopsis

- **Slogan**: Demonstrate separation of user interface from engine

- **Platform**: Windows, compiled with Visual Studio 2022

- **Language**: C++

- **License**: [BSD 3-Clause](./LICENSE.txt) &nbsp; (quick summary on [tldrlegal.com](https://tldrlegal.com/license/bsd-3-clause-license-(revised)))

- **Copyright**: © 2022 Norbert C. Maier

- **Status**: Embryonic. Proof-of-concept, not much to see yet.

## The Components

1. **SneewitLib** — The Library. This is the pure functionality without any user interface.

2. **SneewitCon** — A Windows console in C++, some textual user interface for WittLib —
 The interesting part are the functions, aka 'delegates', it passes
 to the library during initialization

3. **SneewitWin** — A GUI in C++ using the Windows API

*Planned :*

4. *SneewitCs — C# WinForms GUI*

5. *SneewitWpf — C# WPF GUI*

6. *SneewitJav — Java GUI*

7. *SneewitPy — Python GUI*

8. *SneewitPhp — PHP GUI*

## References

Here are some references used while programming :

<img src="./docs/icos/20200616o0738.microsoft.v1.x0048y0048.png" align="left" width="48" height="48" alt="Link icon Microsoft pages">
 <img src="./docs/icos/20220129o0912.space.v0.x0016y0048.png" align="left" width="16" height="56" alt="Spacer Icon">
 For getting the foot in the door with project SneewitLib, we used Microsoft article
 <a href="https://docs.microsoft.com/en-us/cpp/build/walkthrough-creating-and-using-a-dynamic-link-library-cpp" style="font-weight:bold;" data-webshot="20220128°1732" id="">Walkthrough: Create and use your own Dynamic Link Library (C++)</a>
 <sup><sub><sup>*[ref 20220106°1116]*</sup></sub></sup>

&nbsp;

## Credits

<img src="./docs/imgs/20220108o1738.seven_dwarves2.v0.gif" align="right" width="320" height="509" data-dims="x401y637" alt="Seven dwarfs">

<img src="./docs/icos/20211219o1753.brookmiles.v2.x0048y0048.png" align="left" width="48" height="48" alt="Link icon Miles Brook">
 <img src="./docs/icos/20220129o0912.space.v0.x0016y0048.png" align="left" width="16" height="56" alt="Spacer Icon">
 The tutorial codes
 [www.winprog.org/tutorial](http://www.winprog.org/tutorial/) by Miles Brook
 helped with the SneewittWin GUI part. See also project
 [WinApiTut](https://gitlab.com/normai/winapitut).

&nbsp;

<img src="./docs/icos/20220108o1713.disneyclips.v2.x0048y0048.png" align="left" width="48" height="48" alt="Link icon for www.disneyclips.com">
 <img src="./docs/icos/20220129o0912.space.v0.x0016y0048.png" align="left" width="16" height="56" alt="Spacer Icon">
 The top right Snow White portrait image comes from
 [www.disneyclips.com/&#8203;images4/&#8203;snowwhite3.html](https://www.disneyclips.com/images4/snowwhite3.html)
 , it is licensed 'for noncommercial use only'
 <sup><sub><sup>*[ref 20220108°1722]*</sup></sub></sup>

&nbsp;

<img src="./docs/icos/20220108o1713.disneyclips.v2.x0048y0048.png" align="left" width="48" height="48" alt="Link icon for www.disneyclips.com">
 <img src="./docs/icos/20220129o0912.space.v0.x0016y0048.png" align="left" width="16" height="56" alt="Spacer Icon">
 The dwarfs pyramid image on bottom right here comes from
 [www.disneyclips.com/&#8203;images4/&#8203;snowwhite7.html](https://www.disneyclips.com/images4/snowwhite7.html)
 , it is licensed 'for noncommercial use only'
 <sup><sub><sup>*[ref 20220108°1734]*</sup></sub></sup>

&nbsp;

## Finally

Find other projects of mine on
 [github.com/normai/](https://github.com/normai/)

Bye
 <br>Norbert
 <br>2022-Jan-30 &nbsp; <del>2022-Jan-23</del>

<sup><sub><sup>*[file 20220108°0842, sln 20220108°0911]* ܀Ω</sup></sub></sup>
