﻿/**
 *  file       : 20220108°1123 sneewittchen/sneewittcon/main.h
 *  summary    : Definitions and prototypes for main.cpp
 *  copyright  : © 2022 Norbert C. Maier and contributors
 *  license    : BSD 3-Clause (choosealicense.com/licenses/bsd-3-clause/)
 *  encoding   : UTF-8-with-BOM
 */
#ifndef WITTCON_H
#define WITTCON_H

#define SNEEWIT_CON_PROGNAME "Sneewittchen Console"
#define SNEEWIT_CON_VERSION "0.1.3"

#endif // WITTCON_H
