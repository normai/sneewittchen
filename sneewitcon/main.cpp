﻿/**
 *  file       : 20220108°1021 sneewittchen/sneewittcon/main.cpp
 *  summary    : This shall be the console part of the solution
 *  version    :
 *  copyright  : © 2022 Norbert C. Maier and contributors
 *  license    : BSD 3-Clause (choosealicense.com/licenses/bsd-3-clause/)
 *  encoding   : UTF-8-with-BOM
 */

#include <conio.h>                     // Windows specific _getch()
#include <iostream>
#include <string>
#include <sstream>                     // std::stringstream, std::stringbuf // See e.g. www.cplusplus.com/reference/sstream/stringstream/str/
#include "sneewittlib.h"               // Provide the functions in the DLL
#include "main.h"
using namespace std;

void GoFibonacci();
void GoPingSneewittchen();
string Input();                        // Input delegate
void Output(string sOut);              // Output delegate

int main(int argc, char* argv[])
{
   // The old 'cout <<' were possible here, but no more later inside the library.
   // So let's demonstrate the replacement, how it can be done without std::cout.
   // The crucial news to communicate with a library are functions Output() and Input()
   /// cout << "*** Welcome to the " << PROGNAME << " " << PROGVERSION << " ***" << endl;
   stringstream ss;
   ss << "*** Welcome to the " << SNEEWIT_CON_PROGNAME << " " << SNEEWIT_CON_VERSION << " ***" << endl;
   Output(ss.str());

   initialize_Schneewittchen(Output, Input);

   bool bLoop = true;
   char c = ' ';

   // Dispatcher loop
   while (bLoop)
   {
      cout << endl << "Keys: Fibo Ping eXit" << endl;

      c = _getch();
      switch (c)
      {
      case 'f':
         GoFibonacci();
         break;
      case 'p':
         GoPingSneewittchen();
         break;
      case 'x':
         bLoop = false;
         break;
      default:
         std::cout << "Invalid key '" << c << "'" << std::endl;
         break;
      }
   }

   //cout << endl << "Press Enter to exit" << endl;
   //cin.get();
}

/**
 *  function   : 20220108°1421
 *  summary    : Shall serve as a first input facility delegate
 */
string Input() {
   string sIn = "";
   cin >> sIn;
   return sIn;
}

/**
 *  function   : 20220108°1411
 *  summary    : Shall serve as a first output facility delegate
 */
void Output(string sOut) {
   cout << sOut;
}


/**
 *  function   : 20220108°1031
 *  summary    : Calculate a Fibonacci series
 *  origin     : docs.microsoft.com/en-us/cpp/build/walkthrough-creating-and-using-a-dynamic-link-library-cpp [ref 20220106°1116]
 *  todo       : Condense this to one single-line call with the limit as parameter [todo 20220111°2021 'condense']
 */
void GoFibonacci() {

   // Initialize a Fibonacci relation sequence
   fibonacci_init(1, 1);

   // Write out the sequence values until overflow
   cout << "How many Fibonacci sequence values do fit in an unsigned 64-bit integer?";
   do {
      int iNdx = fibonacci_index();
      if (iNdx < 9 or iNdx > 87) {
         cout << endl << " - " << iNdx << " : " << fibonacci_current();
      }
      else {
         static bool bFirst = true;
         if (bFirst) {
            cout << endl << " - .";
            bFirst = false;
         }
         else {
            cout << ".";
         }
      }
   } while (fibonacci_next());

   cout << endl << "Result: " << fibonacci_index() + 1 << " Fibonacci values fit in an unsigned 64-bit int." << endl;
}

/**
 *  function   : 20220122°1611
 *  summary    : Ping
 */
void GoPingSneewittchen() {
   sneewittchen_ping();
}
