﻿/**
 *  file       : 20220108°1233 sneewittchen/sneewittlib/sneewittlib.h
 *  summary    : Definitions and prototypes for wittlib.cpp
 *  copyright  : © 2022 Norbert C. Maier and contributors
 *  license    : BSD 3-Clause (choosealicense.com/licenses/bsd-3-clause/)
 *  encoding   : UTF-8-with-BOM
 *  summary    :
 */
#ifndef WITTLIB_H
#define WITTLIB_H

#define SNEEWITT_LIB_PROGNAME "Sneewittchen Library"
#define SNEEWITT_LIB_VERSION "0.1.3"

#define WIN32_LEAN_AND_MEAN // For precompiler conditon in <Windows.h> to skip rarely-used stuff

// Definition in lib projects settinPROGNAME_SNEEWITT_LIBgs yields different headers for lib and for calling hosts
#ifdef SCHNEEWITLIB_EXPORTS
#define SCHNEEWITLIB_API __declspec(dllexport)
#else
#define SCHNEEWITLIB_API __declspec(dllimport)
#endif

#include <functional>
#include <string>
using namespace std;

// The Sneewittchen API [seq 20220129°1111]
extern "C" SCHNEEWITLIB_API void initialize_Schneewittchen ( function<void(string)> Output
                                                            , function<string()> Input
                                                             );
extern "C" SCHNEEWITLIB_API void sneewittchen_canary1();
extern "C" SCHNEEWITLIB_API void sneewittchen_canary2(string s);
extern "C" SCHNEEWITLIB_API string sneewittchen_canary3(); // Compiler warning "function has C-linkage specified, but returns UDT" [issue 20220208°0751]]
extern "C" SCHNEEWITLIB_API void sneewittchen_ping();
extern "C" SCHNEEWITLIB_API string sneewittchen_version();

// ----------------------------------------------------
// The 4 Functions after the MS DLL-Tutorial [ref ]

// Initialize a Fibonacci relation sequence such that F(0) = a, F(1) = b.
// This function must be called before any other function.
extern "C" SCHNEEWITLIB_API void fibonacci_init(
   const unsigned long long a, const unsigned long long b);

// Produce the next value in the sequence.
// Returns true on success and updates current value and index.
// Returns false on overflow, leaves current value and index unchanged.
extern "C" SCHNEEWITLIB_API bool fibonacci_next();

// Get the current value in the sequence.
extern "C" SCHNEEWITLIB_API unsigned long long fibonacci_current();

// Get the position of the current value in the sequence.
extern "C" SCHNEEWITLIB_API unsigned fibonacci_index();

// ----------------------------------------------------

#endif // WITTLIB_H
