﻿/**
 *  file       : 20220108°1221 sneewittchen/sneewittlib/dllmain.cpp
 *  summary    : This defines the entry point for the DLL application
 *  copyright  : © 2022 Norbert C. Maier and contributors
 *  license    : BSD 3-Clause (choosealicense.com/licenses/bsd-3-clause/)
 *  encoding   : UTF-8-with-BOM
 */

#include <windows.h>
#include "sneewittlib.h"
using namespace std;

BOOL APIENTRY DllMain ( HMODULE hModule
                       , DWORD  ul_reason_for_call
                        , LPVOID lpReserved
                         )
{
   switch (ul_reason_for_call)
   {
      case DLL_PROCESS_ATTACH :
      case DLL_THREAD_ATTACH :
      case DLL_THREAD_DETACH :
      case DLL_PROCESS_DETACH :
         break;
   }
   return TRUE;
}
