﻿/**
 *  file       : 20220108°1231 sneewittchen/sneewittlib/sneewittlib.cpp
 *  summary    : This provides functionality without interface
 *  copyright  : © 2022 Norbert C. Maier and contributors
 *  license    : BSD 3-Clause (choosealicense.com/licenses/bsd-3-clause/)
 *  encoding   : UTF-8-with-BOM
 */
#include <limits.h>
#include <string>
#include <utility>
#include <windows.h> // e.g. MessageBox
#include "sneewittlib.h"
#include "witch_win_utils.h"
using namespace std;

// DLL internal state variables
static unsigned long long previous_;                   // Previous value, if any
static unsigned long long current_;                    // Current sequence value
static unsigned index_;                                // Current seq. position

static function<void(string)> outputFacility;
static function<string()> inputFacility;
void Output(string sOut);
string Input();

/**
 * function : 20220108°1431
 * summary : Initialize library
 * see : E.g. en.cppreference.com/w/cpp/utility/functional/function
 */
void initialize_Schneewittchen(function<void(string)> output, function<string()> input)
{
   outputFacility = output;
   inputFacility = input;

   Output("Schneewittchen-Library is initialized.");
}

/**
 * function : 20220122°1521
 * summary : Provide a library function with minimal error sources
 * ref : stackoverflow.com/questions/21620752/display-a-variable-in-messagebox-c
 *        — This provides code for the bulky temporary character buffer
 * ref : www.cplusplus.com/forum/windows/90074/
 *        — This provides a smaller example with NULL instead hWnd
 */
void sneewittchen_canary1()
{
   //char buff[100];
   //string name = "stackoverflow";
   //sprintf_s(buff, "name is:%s", name.c_str());
   //cout << buff;
   //MessageBox(hWnd, buff, "Msg title", MB_OK | MB_ICONQUESTION);
   MessageBox(NULL, L"Aloha SneewittLib library initialization canary", L"SneewittLib", MB_OK | MB_ICONQUESTION);

   return;
}

/**
 *  function : 20220122°1531
 *  summary : Provide a library canary function taking a parameter
 */
void sneewittchen_canary2(string sCanary)
{
   // Convert std::string to LPCWSTR [seq 20220122°1537] after ref 20220122°1535
   // chain : func 20220122°1421 'Convert c-string to cpp-string'
   if (false) {
      wstring sTmp = wstring(sCanary.begin(), sCanary.end());
      LPCWSTR sw = sTmp.c_str();
      MessageBox(NULL, sw, L"SneewittLib", MB_OK | MB_ICONQUESTION);
   }
   else {
      ////const LPCWSTR sw = conv_cppstring_to_lpcwstr(sCanary);
      wstring sw1 = conv_cppstring_to_lpcwstr(sCanary);
      LPCWSTR sw2 = sw1.c_str();
      ////LPCWSTR sw2 = conv_cppstring_to_lpcwstr(sCanary).c_str(); // Does not work as expected. Why not?!
      MessageBox(NULL, sw2, L"SneewittLib", MB_OK | MB_ICONQUESTION);
   }

   //MessageBox(NULL, sw, L"SneewittLib", MB_OK | MB_ICONQUESTION);
   return;
}

/**
 *  function : 20220122°1541
 *  summary : Provide a library canary function returning a value
 */
string sneewittchen_canary3()
{
   string s = "Greetings from SneewittLib Canary Three";
   return s;
}

/**
 *  function : 20220122°1551
 *  summary :
 */
void sneewittchen_ping()
{
   string s = "SneewittLib has heard your ping";
   Output(s);
}

/**
 *  function : 20220129°1121
 *  summary : Return the version string
 */
string sneewittchen_version()
{
   return SNEEWITT_LIB_VERSION;
}

/**
 * function : 20220108°1441
 * summary : Forwards the output to the output facility, by whichever host that was provided
 */
void Output(string sOut)
{
   outputFacility(sOut);
}

/**
 * function : 20220108°1451
 * summary : Retrieves input from the input facility, by whichever host that was provided
 */
string Input()
{
   return inputFacility();
}

/*
   todo 20220129°1131 'Easy single fibo call'
   matter : So far, the Fibo caller has to have serveral calls in a
      loop with conditions. This should be one single call, possibly
      with a parameter. The output still has to come line by line.
   status : Open.
*/

// ====================================================
// Fibonacci [seq 20220106°1212]
// sum : The four functions after tutorial "Walkthrough: Create and use your own Dynamic Link Library (C++)"
// ref : docs.microsoft.com/en-us/cpp/build/walkthrough-creating-and-using-a-dynamic-link-library-cpp [ref 20220106°1116]
// ref : screenshot 20220108°1312 'Create new DLL project'

// func 20220106°1213
// Initialize a Fibonacci relation sequence such that F(0) = a, F(1) = b.
// This function must be called before any other function.
void fibonacci_init ( const unsigned long long a       //
                     , const unsigned long long b      //
                      )
{
   index_ = 0;
   current_ = a;
   previous_ = b;                                      // See special case when initialized
}

// func 20220106°1214
// Produce the next value in the sequence. Returns true on success, false on overflow.
bool fibonacci_next()
{
   // Check to see if we'd overflow result or position
   if ( (ULLONG_MAX - previous_ < current_)
       || (UINT_MAX == index_)
        ) {
      return false;
   }

   // Special case when index == 0, just return b value
   if (index_ > 0) {
      previous_ += current_;                           // Otherwise, calculate next sequence value
   }
   std::swap(current_, previous_);
   ++index_;
   return true;
}

// func 20220106°1215
// Get the current value in the sequence.
unsigned long long fibonacci_current() {
   return current_;
}

// func 20220106°1216
// Get the current index position in the sequence.
unsigned fibonacci_index() {
   return index_;
}
// ====================================================
