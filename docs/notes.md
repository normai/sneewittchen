﻿

   version 20220122°1631 — 'GUI output delegate works'
   • See line 20220122°1621 file 20220122°1321 SneewittWin/main.cpp

   sprint 20220122°1511 'Access library from VS created project'
   do : Try using project 20220122°1311 SneewittWin to access the library
   reason : Perhaps this one has better code and project settings
   steps : (1) Adjustments for proof-of-concept
    • Solution Explorer → References → Add 'wittlib's
    • Project Properties → Additional Directories → Add '..\wittlib\;'
    • In main CPP file add #include "wittlib.h" // [sprint 20220122°1511`03]'
    • Introduce func 20220122°1521 void sneewittchen_canary1()
    • Introduce func 20220122°1531 void sneewittchen_canary2(string s)
    • Introduce func 20220122°1541 string sneewittchen_canary3()
    • Heureka! Library access works with parameters forth and back.
   steps : (2) Adjustments for delegates
    • Insert delegate preparation block 20220111°2007
    • Activate "initialize_Schneewittchen(Output, Input);" line 20220111°2031
    • line 20220122°1621 Output delegate works!
   status : Heureka
   ܀

   version 20220122°1431 'Commit anyway'
   text : After adjustions, issue 20220122°1211 'Debug assertion failed in
           debug_heap.cpp' still persists. But I want do a commit anyway.
   ܀

   issue 20220122°1411 'Error C2664 cannot convert to LPCWSTR'
   matter : Compiler error "Error C2664 'int MessageBoxW(HWND,LPCWSTR,LPCWSTR,UINT)':
      cannot convert argument 2 from 'const char [16]' to 'LPCWSTR'
      d03_menu_one G:\work\trekta\demoscpp\trunk\schneewittchen\d03_menu_one\d03_menu_one.cpp
   chain : Screenshot 20220122o1412.vs--d03-menu-one--error-c2664.png
   ref : https://stackoverflow.com/questions/19715144/how-to-convert-char-to-lpcwstr [ref 20220122°1412]
   status : Closed. Can be fixed by prefix 'L'
   ܀

   issue 20220122°1231 'MBCS versus Unicode char set'
   matter : Have detected difference in project properties in
      'Character set' setting
   chain : Screenshot 20220122°1232 Project WittCon has 'Unicode'
   chain : Screenshot 20220122°1233 Project d03_menu_one has 'Mulit-Byte'
   chain : Screenshot 20220122°1234 Project d03_menu_one changing
   chain : Screenshot 20220122°1235 Project d03_menu_one changed
   chain : Screenshot 20220122°1236 Project d03_menu_one adjust 'Use Debug Libraries'
   ref : MS Social thread 'Unicode Character Set vs Multi-Byte Character Set'
          https://social.msdn.microsoft.com/Forums/en-US/9873accb-9eb6-4303-b4a7-39ed6cffea99/unicode-character-set-vs-multibyte-character-set
          [ref 20220122°1242] note: Search term '"multi-byte character set" versus "unicode character set"'
          yields very few hits. Search term 'mbcs versus "unicode character set"' yields more
   ref : Codeproject article 'The Complete Guide to C++ Strings, Part I - Win32 Character Encodings' [ref 20220122°1243]
          https://www.codeproject.com/Articles/2995/The-Complete-Guide-to-C-Strings-Part-I-Win32-Chara
   ref : Stackexchange thread 'What is the difference between "Wide character" set and "Unicode character set"? [closed]' [ref 20220122°1244]
          https://softwareengineering.stackexchange.com/questions/294388/what-is-the-difference-between-wide-character-set-and-unicode-character-set'
   finding : After adjusting above settings in d03_menu_one ..
   status : Probably closed.
   ܀

   issue 20220122°1211 'Debug assertion failed in debug_heap.cpp'
   matter : Fatal error when line 20220111°2031 "initialize_Schneewittchen(Output, Input);"
      is open. Then, the window is paint and the error dialog appears,
      and there is no chance to set a breakpoint.
   chain : Screenshots 20220122°1212 and 20220122°1213
   ref : https://docs.microsoft.com/en-us/visualstudio/debugger/crt-debug-heap-details?view=vs-2022 [ref 20220122°1214]
   ref : https://docs.microsoft.com/en-us/visualstudio/debugger/crt-debugging-techniques?view=vs-2022 [ref 20220122°1215]
   ref : https://docs.microsoft.com/en-us/cpp/c-runtime-library/potential-errors-passing-crt-objects-across-dll-boundaries?view=msvc-170 [ref 20220122°1216]
   quest : Might this compiler switch help "/MD link with MSVCRT.LIB"? When doing so,
      it causes warning "Overriding /MTd", that switch is"/MTd link with LIBCMTD.LIB debug lib".
   status : Open
   ܀

   log 20220122°1111 'Try using function from d16_font_one.c'
   sum : After project .. builds, with the library attached, I want
 provide some output means, for which I choose file 20220102°1611
 winapitut/d16_font_one/d16_font_one.c to exploit.
   action : Inserted a function from there into d03_menu_one.cpp
   Steps (to be completed afte above issues) :
    • Take over func 20220122°1121 DrawClientSize
    • ..
   ܀

   log 20220121°1821 'Try it with d03_menu_one'
   summary : So far, loading the library fails with a hard to find
      exception. I suspect, it has to do with the fact, that in
      project 20220108°1111 WittGui, we use a Windows dialog box, not
      an ordinary window. So I want to try it with an ordinary window.
   The steps to integrate project d03_menu_one were:
    • Insert project d03_menu_one
    • Add reference from d03_menu_one to wittlib
    • Project settings 'Additional includes' supplement `..\wittlib\;`
    • In d03_menu_one.c supplement `#include "wittlib.h"`
       this will not run, since wittlib.h code, namely in the initialization func params
    • Rename d03_menu_one.c to Rename d03_menu_one.cpp
    • Adjust Platform type from 'Win32' to that of lib 'x64'
    • Now it builds
   status : No, the initial suspicion was wrong.
   ܀

   —————————————————————— Issues ——————————————————————

   issue 20220108°1751 'Convert C to CPP'
   matter : Compiler has type issues
   note : Compiler error : C2664 "'INT_PTR DialogBoxParamA(HINSTANCE,LPCSTR,HWND,DLGPROC,LPARAM)': cannot convert argument 4 from 'BOOL (__cdecl *)(HWND,UINT,WPARAM,LPARAM)' to 'DLGPROC'"
   ref : Solution found in [ref ]
          https://stackoverflow.com/questions/31113172/transformation-of-c-code-from-32-bit-to-64-bit
   finding : It is not a C/C++ issue but a 32-bit/64-bit issue
   status : Closed in v0.1.0
   ܀

   ———————————————————— References ————————————————————


   ref 2022xxxx°xxxx
   pagetype    :
   url         :
   title       :
   review      :
   icon        :
   note        :
   ܀

   ref 20220123°0843
   pagetype    : article
   url         : https://docs.microsoft.com/en-us/windows/win32/apiindex/windows-api-list
   title       : Windows API index
   review      : Nice list. Just there is nothing like 'TextBox'
   icon        :
   note        :
   ܀

   ref 20220123°0842
   pagetype    : thread
   url         : https://stackoverflow.com/questions/7598067/how-to-create-a-windows-style-textbox-in-a-c-win32-application
   title       : How to create a Windows-style textbox in a C++ Win32 application
   review      :
   icon        :
   note        : Mentions a Spy++ tool, which is probably spyxx.exe
   ܀

   ref 20220111°2051
   pagetype    : MSDN thread
   url         : https://social.msdn.microsoft.com/Forums/vstudio/en-US/99354676-56c4-48b7-be62-ec34d53a073f/how-to-write-text-to-desktop-wallpaper
   title       : How to write text to desktop wallpaper
   review      : Found while searching for 'HWND hwnd'
   icon        :
   note        :
   ܀

   ref 20220111°1822
   pagetype    : thread
   url         : https://stackoverflow.com/questions/31113172/transformation-of-c-code-from-32-bit-to-64-bit
   title       : Transformation of C++ code from 32 bit to 64 bit
   review      : Yields solution for issue 20220108°1751 'Convert C to CPP'
   note        :
   ܀

   ref 20220108°1613
   pagetype    : product
   url         : http://www.angusj.com/resourcehacker/
   title       : Resource HackerTM
   slogan      : A freeware resource compiler & decompiler for Windows® applications
   review      :
   chain       : dld 20220108°1611
   icon        : ico 20220108°1616
   note        :
   ܀

   ref 20220108°1612
   pagetype    : thread
   url         : https://stackoverflow.com/questions/381915/free-resource-editor-for-windows-rc-files
   title       : Free resource editor for Windows .rc files?
   review      :
   note        :
   ܀

   ——————————————— Story 20220108°1341 ————————————————

   About the library project

   The DLL was created after Microsoft-Artikel
   "Walkthrough: Create and use your own Dynamic Link Library (C++)"
   https://docs.microsoft.com/en-us/cpp/build/walkthrough-creating-and-using-a-dynamic-link-library-cpp
   [ref 20220106°1116]

   Cornerstone snippets from above article:

   • Windows requires extra information that isn't part of the standard
   C++ compilation model to make these connections. The MSVC compiler
   implements some Microsoft-specific extensions to C++ to provide this
   extra information.

   • To avoid out-of-sync code, we recommend you set the include path in
   your client project to include the DLL header files directly from your
   DLL project. Also, set the library path in your client project to include
   the DLL import libraries from the DLL project. And finally, copy the
   built DLL from the DLL project into your client build output directory.
   This step allows your client app to use the same DLL code you build.

   • To add the DLL header to your include path ...
      [screenshots 20220108°1324/°1324/°1325]

   • ...

   ——————————————— Story 20220108°1351 ————————————————

   About the project name

   While the project planning phase, for some reason someone mentioned
 the 'The seven dwarfs behind the seven mountains'. I thought: Seven? Like
 six students plus one lecturer? That fits. And the story behind around
 the seven dwarfs is Snow White.

   Hence the project name Schneewittchen. C++ may be Snow White, and
 the the team members are the seven dwarfs. Or the library is Snow White
 and the user interfaces are the dwarfs.

   Then about the shortening from 'Schneewittchen' to 'Sneewittchen',
 I did not come up with this myself, but have read it on a poster for
 the 1937 Walt Disney cinema film.

   ———————————————————————
   [file 20220108°0844] ܀Ω
