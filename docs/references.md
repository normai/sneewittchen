﻿# References

<img src="./icos/20200616o0738.microsoft.v1.x0048y0048.png" align="left" width="48" height="48" alt="Link icon Microsoft pages">
 <img src="./icos/20220129o0912.space.v0.x0016y0048.png" align="left" width="16" height="56" alt="Spacer Icon">
 The EditBox shows up in the form but has trouble with newlines.
 Perhaps Microsoft-Support article
 <a href="https://support.microsoft.com/en-us/topic/how-to-use-the-enter-key-from-edit-controls-in-a-dialog-box-a520c533-1a62-f51c-af93-c7f978bc1080" data-webshot="20220129°1032" id="">How To Use the ENTER Key from Edit Controls in a Dialog Box</a>
 can help for this.

&nbsp;

<img src="./icos/20110921o1121.stackoverflow.v1.x0048y0048.gif" align="left" width="48" height="48" alt="Link icon for StackOverflow">
 <img src="./icos/20220129o0912.space.v0.x0016y0048.png" align="left" width="16" height="56" alt="Spacer Icon">
 StackOverflow thread
 <a href="https://stackoverflow.com/questions/3773916/what-is-the-simplest-way-to-create-edit-box-in-c" data-webshot="20220129°1012" id="">What is the simplest way to create edit box in C++</a>
 . Usage: func 20220129°1021 'Dynamically create TextBox'.

&nbsp;

&nbsp;

<img src="./icos/20200616o0738.microsoft.v1.x0048y0048.png" align="left" width="48" height="48" alt="Link icon Microsoft pages">
 <img src="./icos/20220129o0912.space.v0.x0016y0048.png" align="left" width="16" height="56" alt="Spacer Icon">
 Possibly useful example solution in Microsoft Docs article
 <a href="https://docs.microsoft.com/en-us/windows/win32/learnwin32/example--the-open-dialog-box" data-webshot="20220129°0941" id="">Example: The Open Dialog Box</a>
 <sup><sub><sup>*[ref 20220129°0941]*</sup></sub></sup>
 with file provided on Github
 <sup><sub><sup>*[ses 20220129o0940]*</sup></sub></sup>

&nbsp;

<sup><sub><sup>*[file 20220108°0847]* ܀Ω</sup></sub></sup>
