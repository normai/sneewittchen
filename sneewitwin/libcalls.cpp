﻿// file : 20220126°1731 sneewittchen/sneewittwin/libcalls.cpp
// sum : This module shall collect the functions which call the library
// encoding : UTF-8-with-BOM

#include <string>
#include "framework.h"
#include "sneewittlib.h"
#include "main.h"                              // [sprint 20220122°1511`03]
using namespace std;


// ====================================================
// Delegate preparation block 20220111°2007

/**
 *  function   : 20220108°1421
 *  summary    : Shall serve as a first input facility delegate
 */
string Input() {
   string sIn = "This shall be some input.";
   return sIn;
}

/**
 *  func : 20220111°2041
 */
void Output(string sOut) {
   std::wstring sTmp = std::wstring(sOut.begin(), sOut.end()); // Conversion after seq 20220122°1537
   LPCWSTR sw = sTmp.c_str();
   MessageBox(NULL, sw, L"SneewittGui", MB_OK | MB_ICONQUESTION);
}
// ====================================================


// function 20220126°1741
void GoCanaries() {

   //-------------------------------------------------
   // Sequence added with sprint 20220122°1511 'Access library from VS created project'
   // This are simple library methods, which can be called before library initialization

   // Probe the library [line 20220122°1523]
   // heureka : No more issue 20220122°1211 'Debug assertion failed in debug_heap.cpp'?
   if (false) {                                        // toggle
      sneewittchen_canary1();
   }

   // Probe two [line 20220122°1533]
   if (true) {                                        // toggle
      string s1 = "Aloha Canary Two";
      sneewittchen_canary2(s1);
   }

   // Probe three [line 20220122°1543]
   if (false) {                                        // toggle
      string s2 = sneewittchen_canary3();
      std::wstring sTmp = std::wstring(s2.begin(), s2.end()); // Conversion after seq 20220122°1537
      LPCWSTR sw = sTmp.c_str();
      MessageBox(NULL, sw, L"SneewittGui", MB_OK | MB_ICONQUESTION);
   }

   // line 20220111°2031
   // Heureka, no more issue 20220122°1211 'Debug assertion failed in debug_heap.cpp' [note 20220122°1545]
   initialize_Schneewittchen(Output, Input);

   // line 20220122°1621 Heureka -- Output delegate works
   sneewittchen_ping();
   //-------------------------------------------------
}
