﻿// file : 20220122°1331 sneewittchen/sneewittwin/framework.h
// sum : File was provided in the VS2022 project template with the note "Include
//    file for standard system include files, or project specific include files".
// encoding : UTF-8-with-BOM

#pragma once

#include "targetver.h"
#define WIN32_LEAN_AND_MEAN // For precompiler conditon in <Windows.h> to skip rarely-used stuff
#include <malloc.h>
#include <memory.h>
#include <stdlib.h>
#include <tchar.h>
#include <windows.h>
