﻿// file 20220126°1723 sneewittchen/sneewittwin/apicalls.cpp
// encoding : UTF-8-with-BOM

#ifndef APICALLS_H
#define APICALLS_H

#include "framework.h"

INT_PTR CALLBACK GoAbout(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);

#endif // APICALLS_H
